Balkon projekt
==============

Ládás kert
----------

Az ötlet a [Kertkaland](kertkaland) című könyvből van, csak jóval kisebb
volumenben tervezzük "bujásítani" a balkonunkat.

**Hol?**  
Szeged, Makkosháza, panel, 3. emelet, majdnem pontosan délre néző erkéllyel.

**Mikor?**  
Minél hamarabb, amíg még be tudjuk szerezni az eszközöket

**Cél**  
Nulla kertészelőélettel valami ehetőt varázsolni a balkonon.

**Épített környezet**  
TODO

**Napjárás**  
Az erkély kb. délnek néz egy játszótérre, trapéz (felfelé keskenyedő)
oldalpanelekkel határolt, bal oldalon szomszédos épület takar egy kicsit
balról. A játszóér körül ötemeletesek, távolabb srégen balra (ha kinézek az
erkélyről) egy tízemeletes (nem tűnt fel, hogy árnyékolna). SunCalcon a
hozzávetőlegesen beállított hellyel [így][suncalc] néz ki a történet
(2020-03-30-as dátummal).


**Rendelkezésre álló eszközök**

- Basic barkácscuccok

**Tervezett eszközök, nyersanyagok** (méretek milliméterben, h x sz x m)

- 60 l-es habarcsláda x 2 (710 x 300 x 400)
- balkonláda alátét (vízelvezetés)
- kavicsok a ládák aljába (pangó víz ellen)

**Tervezett vetemények**  
Bánvölgyi Helga [gyűjtése][novenytars] alapján próbáltunk egymással jól kijövő
növényeket társítani egy-egy ládába.

- Láda No. 1: paradicsom + bazsalikom + hagyma + petrezselyem
- Láda No. 2: saláta + mángold + rukkola

Gilisztakomposzt (3-rétegű, vödrös)
----------------------------------------

**Eszközök, giliszták**  
Fúrón és 6 mm-s fúrószárón kívül ezek kellenek:

- 3 OBI-s vödör (10l)
- 1 OBI-s vödör fedél
- 2 kicsi befőttes üveg (kb. 10 cm magas, távtartónak)
- 20 dkg giliszta

Gyakorlatilag a [Building a Stackable DIY Worm Farm for \$30][gilivideo]
YouTube-videó és a [How to Make Your Own Worm Farm][gilikompi] cikk alapján
raktam össze a gilisztakomposztálót. A felső két vödör alját kb. 4 cm-enként
megfúrtam, illetve a vödrök perem alatt nagyjából 10 cm-re ugyanilyen furatok
vannak körben, úgy 5 cm-enként. Itt arra kell vigyázni csak, hogy ha
összecsúsztatjuk a vödröket közöttük a befőttesüvegekkel, akkor látszódjanak a
furatok a szellőzés végett. Összerakod a vödröket (a legalsó vödröt nem fúrtad
ki), jöhetnek a bentlakók.

Mi 20 dkg gilisztával indítottunk, mert úgy számoltunk, hogy átlagosan napi 10
dkg zöldséghéj, stb. keletkezik a konyhánkon (elvileg a giliszták naponta a
tömegük felét tudják elfogyasztani). A gilisztákat némi földdel együtt kaptuk.
A vödör aljába ment egy kis összetépett újságpapír megnedvesítve, erre jöttek a
giliszták a földjükkel, plusz egy kis zöldségföld és rá egy kis répahéj,
almacsutka (hogy éppen fedje a földet).

**Megjegyzések**

- Kurgi szerint 100 giliszta elég lehet beröffenteni a rendszert.
- A [COMPASTOR][compastor] cég elvileg elküldi a gilisztákat.

**Linkek**

- Az eredeti ötlet [infografikán][giliinfographic].
- A gilisztákat [innen][giliforras] rendeltük.
- Troubleshooting 1: [Worm Bin Problems for Beginners][gilitrbl1].
- Troubleshooting 2: [Composting with Worms: Don’t Make These Five Mistakes][gilitrbl2].

TODO
----

- SketchUpban a balkont rendesen modellezni, ld! **Épített környezet**.

Hivatkozások
------------

- Dóra Melinda Tünde: [Kertkaland][kertkaland], 2020, Animus.
- Bánvölgyi Helga: [Hasznos növénytársítások][novenytars]

[kertkaland]: https://animuscentral.hu/konyv/kertkaland-termelj-magadnak-zoldseget
[suncalc]: https://www.suncalc.org/#/46.2714,20.1501,18/2020.03.30/11:34/1/3
[novenytars]: https://docs.google.com/document/d/1Iv4iWd319VS7Ai13scselJE1Whpy_EJJCATxbu5rLvo/edit
[compastor]: https://compastor.hu/termek/compastor-giliszta/
[gilikompi]: http://working-worms.com/how-to-make-your-own-worm-farm/
[giliinfographic]: http://graphisme-environnement.blogspot.com/2007/06/panneaux-infos-pour-les-jardins-du.html
[gilivideo]: https://www.youtube.com/watch?v=UaajjQ0FhM4
[giliforras]: https://giliszta.webnode.hu/rolunk/
[gilitrbl1]: https://www.tenthacrefarm.com/worm-bin-mistakes-problems/
[gilitrbl2]: https://unclejimswormfarm.com/composting-worms-mistakes/
