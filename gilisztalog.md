Gilisztakomposztáló log
=======================

2020-04-01
----------

Összeállítottuk a gilisztakomposztálót: 3 db 10l-es narancssárga vödör, egy
fedő. A két felső vödrön 6 mm-es furatok a fenekeken meg a körbe a tetejük
alatt úgy 5 cm-rel. Kb. 10-11 cm-es kis befőttes üvegek a távtartók, hogy ne
csússzanak össze.

Összetéptünk egy kievés újságpapírt, benedvesítettük, majd jött 20 dkg giliszta
a földjével, meg egy kicsi OBI-s paradicsom- és zöldségföld.  KÉsőn vettük
észre, hogy ennek a pH-ja 6-os, állítólag ezt még elviselik, de nem ez a
kedvencük, ld! [itt][ph]. Kaptak még egy kicsi zöldséghéjat.

Estére letakartuk őket (a szobában).

2020-04-02
----------

Reggel jó néhányuk fent volt a vödör fedelének a szélénél, mintha befészkeltek
volna.

Napközben bespricceltem hozzájuk, mert száraznak láttam a felszínt.  Délután
átforgattuk őket óvatosan kézzel, száraznak tűnt a felszínhez közeli rész,
alaposan bespricceltük őket. A giliszták egy csomóban a "föld" közepén voltak
többnyire.

Letakartuk őket egy kicsit, erre egy óra múlva már négyen megindultak fölfelé a
vödör falán, inkább levettük a törölközőt.

2020-04-06
----------

Nem kaptak az üzembe helyezés óta kaját, tegnap kicsit bespricceltem. Egy két
jószág megszökött, reggel kiszáradva találtuk meg őket. Éjszakára a nappaliban
volt, de reggel nem éreztem semmilyen szagot.

[ph]: https://wormfarmguru.com/ph-worm-bin-acidity/
